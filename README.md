# SATIE documentation website

SATIE version 1.6.8 - documentation overview.

This is the repo for the SATIE documentation website prototype.

You can [access the SATIE documentation website here](https://sat-mtl.gitlab.io/documentation/satie) and [here are the SATIE repos on Gitlab](https://gitlab.com/sat-mtl/tools/satie), the [main project being this one](https://gitlab.com/sat-mtl/tools/satie/satie).

For the *complete documentation*, see the SuperCollider help files in the SuperCollider IDE.

## License

This documentation is licensed under a [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/)

All the source code (including examples and excerpt from the software) is licensed under the [GNU Public License version 3 (GPLv3)](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Code of Conduct

By participating in this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md). We expect all contributors to follow the [Code of Conduct](CODE_OF_CONDUCT.md) and to treat fellow humans with respect. It must be followed in all your interactions with the project.

## Installing the documentation

All the instructions are in `INSTALL.md`

## Contributing to the documentation

All the information is in `CONTRIBUTING.md`


