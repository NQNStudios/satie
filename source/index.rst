SATIE
=====

.. warning::
    :name: index-warning

    We are in the process of improving the documentation. You can :doc:`test it here <contents>`, and you are very welcome to signal issues with the documentation `on the issue tracker <https://gitlab.com/sat-mtl/documentation/satie/-/issues>`__.

    The current, complete documentation for SATIE is available in SuperCollider.

.. container:: index-locales

   `En <../en/index.html>`__ -=- `Fr <../fr/index.html>`__

.. container:: index-intro

   welcome to the SATIE documentation website

.. container:: index-menu

   `-` :doc:`documentation (test version) <contents>` - `gitlab <https://gitlab.com/sat-mtl/tools/satie/satie>`__- `about us <https://sat.qc.ca/fr/metalab>`__ - `get in touch <https://gitlab.com/sat-mtl/documentation/satie/-/issues>`__ -

.. container:: index-section

    what is SATIE?

SATIE is an audio spatialization engine developed for realtime rendering of dense audio scenes to large multi-channel loudspeaker systems.

In the following demo video, we can see streams of balls falling from the ceiling towards the ground. In the middle of this stream is a rotating, rigid square that sometimes changes the balls' trajectories. As the camera moves around, we can hear the change in the rendering of the audio scene. Watch here:

.. raw:: html

  <div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/157959672?color=8cc747" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="fullscreen; picture-in-picture" allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

Its aim is to facilitate using 3D space in audio music/audio composition and authoring and to play well with 3D game engines (so far it has been used with Blender and Unity3D) and could also serve as volumetric audio spatialization addition to more traditional desktop DAW systems.

.. container:: index-section

   how does it work?

It is a lower-level audio rendering process that maintains a dynamic DSP graph which is created and controlled via OSC messages from an external process. SATIE’s modular development environment provides for optimized real-time audio scene and resource management. There is no geometry per se in SATIE, rather, SATIE maintains a DSP graph of source nodes that are accumulated to a single « listener », corresponding to the renderer’s output configuration (stereo and/or multi-channel).

.. container:: index-section

   table of contents

* how to :doc:`install SATIE <installation/contents>`
* :doc:`a first tutorial <tuto/basics>` to get you started
* :doc:`tutorials <tuto/contents>` to grow your skills
* :doc:`cheatsheet <tuto/cheatsheet>` for some frequently used commands

.. container:: index-section

   please show me the code!

For more information visit `the code repository <https://gitlab.com/sat-mtl/tools/satie/satie>`__.

.. container:: index-section

   sponsors

This project is made possible thanks to the `Society for Arts and Technologies <https://sat.qc.ca/en/>`__ (also known as SAT).
