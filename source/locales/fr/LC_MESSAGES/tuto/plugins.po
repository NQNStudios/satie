# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2021, Metalab
# This file is distributed under the same license as the SATIE documentation
# package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2021.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: SATIE documentation \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-01 09:39-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.9.0\n"

#: ../../source/tuto/plugins.rst:4
msgid "SATIE Plugins"
msgstr ""

#: ../../source/tuto/plugins.rst:6
msgid "Writing audio plugins for SATIE."
msgstr ""

#: ../../source/tuto/plugins.rst:9
msgid "Description"
msgstr ""

#: ../../source/tuto/plugins.rst:11
msgid ""
"SATIE plugins are SuperCollider functions. They are later wrapped "
"together in order to generate several synths. There are three categories "
"of plugins:"
msgstr ""

#: ../../source/tuto/plugins.rst:14
msgid "Audio plugins"
msgstr ""

#: ../../source/tuto/plugins.rst:16
msgid ""
"Are inserted into the signal chain and provide either a sound source or "
"effect. Usually they generate audio signals. The following families of "
"plugins fall into this category:"
msgstr ""

#: ../../source/tuto/plugins.rst:18
msgid "sources (generators)"
msgstr ""

#: ../../source/tuto/plugins.rst:19
msgid "effects"
msgstr ""

#: ../../source/tuto/plugins.rst:20
msgid "post-processors"
msgstr ""

#: ../../source/tuto/plugins.rst:21
msgid "spatializers"
msgstr ""

#: ../../source/tuto/plugins.rst:24
msgid "Mappers"
msgstr ""

#: ../../source/tuto/plugins.rst:26
msgid ""
"Their purpose is to apply mappings (i.e. via functions) to audio plugins'"
" parameters"
msgstr ""

#: ../../source/tuto/plugins.rst:29
msgid "Analysers"
msgstr ""

#: ../../source/tuto/plugins.rst:31
msgid ""
"Plugins that are inserted into audio plugins signal path, before "
"spatialization stage, and their sole purpose is to analyse some signal "
"feature (i.e. envelope, pitch, etc.) and forward it via OSC. See OSC-API "
"for the OSC message description. These plugins should not return any "
"audio but should make use of SendTrig and SendReply UGens to send "
"triggers or streams of desired information."
msgstr ""

#: ../../source/tuto/plugins.rst:45
msgid "​"
msgstr ""

#: ../../source/tuto/plugins.rst:60
msgid "Plugins in depth"
msgstr ""

#: ../../source/tuto/plugins.rst:62
msgid ""
"SATIE comes with a small set of plugins, and additional plugins can "
"easily be developped and added to the engine. Satie looks for plugins in "
"various locations. See SatiePlugin for a discussion of plugin locations "
"and plugin directory structure."
msgstr ""

#: ../../source/tuto/plugins.rst:64
msgid "Every plugin should follow the following format:"
msgstr ""

#: ../../source/tuto/plugins.rst:84
msgid ""
"Each plugin resides in its own .scd file. Each field is a global variable"
" which gets consumed by the plugin loader, one at a time. It's "
"recommended that the ~name field matches the plugin's file name."
msgstr ""

#: ../../source/tuto/plugins.rst:86
msgid "The process of turning a plugin into a synth is as follows:"
msgstr ""

#: ../../source/tuto/plugins.rst:88
msgid "Register the plugin with the appropriate dictionary in SatieConfiguration"
msgstr ""

#: ../../source/tuto/plugins.rst:89
msgid "Let SatieFactory wrap it into a SynthDef"
msgstr ""

#: ../../source/tuto/plugins.rst:90
msgid "Instantiate the pluginName"
msgstr ""

#: ../../source/tuto/plugins.rst:91
msgid "And finally interact with it"
msgstr ""

#: ../../source/tuto/plugins.rst:94
msgid "Registering the plugin"
msgstr ""

#: ../../source/tuto/plugins.rst:96
msgid ""
"SATIE needs to know about the plugins it manages. The plugins files are "
"kept in directories which organize them by family. They are loaded at "
"configuration time and kept in the following SatieConfiguration members:"
msgstr ""

#: ../../source/tuto/plugins.rst:98
msgid ""
"SatieConfiguration: -sources Typpically audio generators. They are "
"usually placed in a group at \\head"
msgstr ""

#: ../../source/tuto/plugins.rst:101
msgid ""
"SatieConfiguration: -effects Effects, that can be fed signal from "
"generators (placed at \\tail)"
msgstr ""

#: ../../source/tuto/plugins.rst:104
msgid ""
"SatieConfiguration: -spatializers Spatialisers (typically VBAP and other "
"types of amplitude panning)"
msgstr ""

#: ../../source/tuto/plugins.rst:107
msgid "SatieConfiguration: -mappers Parameter mapping."
msgstr ""

#: ../../source/tuto/plugins.rst:110
msgid ""
"SatieConfiguration: -postprocessors Post processing, or mastering. These "
"are ensured to be at the end of the processing chain, between the above "
"plugins and the physical interface."
msgstr ""

#: ../../source/tuto/plugins.rst:113
msgid ""
"SatieConfiguration: -monitoring These are side-chains that can be "
"inserted amongst audio generators or effects in order to monitor the "
"signal and, optionally, send OSC trigger messages back to the server."
msgstr ""

#: ../../source/tuto/plugins.rst:116
msgid "SatieConfiguration: -hoa Higher-order Ambisonics decoders."
msgstr ""

#: ../../source/tuto/plugins.rst:119
msgid ""
"The registration is done by means of loading the plugin code by "
"SatiePlugins and its methods. The source code of SatieConfiguration: "
"-loadPluginDir shows how this is done."
msgstr ""

#: ../../source/tuto/plugins.rst:121
msgid ""
"The plugins are compiled into SynthDefs via Satie: -makeSynthDef. This "
"method will look up the function definition in the indicated dictionary "
"address and prepare a SynthDef associated with provided symbol ID. It "
"will then place the compiled SynthDef in renderer's dictionary where it "
"will be ready for instantiation."
msgstr ""

#: ../../source/tuto/plugins.rst:123
msgid ""
"And finally, we instantiate the desired Synth with Satie: -makeInstance "
"so that we can start interacting with it."
msgstr ""

#: ../../source/tuto/plugins.rst:125
msgid ""
"Note, that by default, the above steps including the compilation of "
"SynthDefs are done automatically by SATIE at boot time, unless overriden "
"in configuration (see variable SatieConfiguration: -generateSynthdefs)."
msgstr ""

#: ../../source/tuto/plugins.rst:128
msgid "Special use plugins"
msgstr ""

#: ../../source/tuto/plugins.rst:131
msgid "Kamikaze"
msgstr ""

#: ../../source/tuto/plugins.rst:133
msgid ""
"SATIE compiles additional \"copies\" of generator and effect plugins. It "
"will create \"shadow\" suffixed with -kamikaze. These are exact copies of"
" the provided code, except that the synth will free itself when silence "
"is detected. These kinds of synths are intended to be used in situations "
"where we don't want to keep track of them. They could be used, for "
"instance, to accompany particle systems."
msgstr ""

#: ../../source/tuto/plugins.rst:136
msgid "Ambi"
msgstr ""

#: ../../source/tuto/plugins.rst:138
msgid ""
"The Ambi shadows are created only when SATIE is configured to use Higher-"
"order Ambisonics. All plugins will then be compiled with copies that are "
"usable with each ambisonics order specified."
msgstr ""

#: ../../source/tuto/plugins.rst:141
msgid "Working with plugins on-the-fly"
msgstr ""

#: ../../source/tuto/plugins.rst:143
msgid ""
"When developing new plugins, it is sometimes desirable to be able to "
"doodle on a scratchpad without the need of saving and loading files off "
"the disk. SatiePlugins: -addAudioPlugin allows us to load a plugin "
"defined in an environment, mimicking the above plugin creation "
"guidelines:"
msgstr ""

